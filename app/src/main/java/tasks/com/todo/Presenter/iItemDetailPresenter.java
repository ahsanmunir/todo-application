package tasks.com.todo.Presenter;

import android.content.Context;

import tasks.com.todo.Model.Items;

public interface iItemDetailPresenter {

    void loadDetails(String itemId);
    void updateDetails(int id, String title, String cost, String location, String description, String imagePath);
    void deleteById(String id);
    void selectImageFromCamera(Context context);
    void selectImageFromGallery(Context context);
}
