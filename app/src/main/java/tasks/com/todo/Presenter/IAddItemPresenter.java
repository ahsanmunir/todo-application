package tasks.com.todo.Presenter;

import android.content.Context;

import tasks.com.todo.Model.Items;
import tasks.com.todo.View.AddItemsView;

public interface IAddItemPresenter {

    void saveData(String title, String cost, String location, String description, String imagePath);

    void selectImageFromCamera(Context context);

    void selectImageFromGallery(Context context);
}
