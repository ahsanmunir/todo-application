package tasks.com.todo.Presenter;

import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.mlsdev.rximagepicker.RxImageConverters;
import com.mlsdev.rximagepicker.RxImagePicker;
import com.mlsdev.rximagepicker.Sources;

import java.io.File;

import tasks.com.todo.Database.ItemsRepository;
import tasks.com.todo.Model.Items;
import tasks.com.todo.View.ItemDetailsView;

public class ItemDetailPresenter implements iItemDetailPresenter {

    ItemDetailsView itemDetailsView;
    ItemsRepository itemsRepository;

    public ItemDetailPresenter(ItemDetailsView itemDetailsView, Application application) {
        this.itemDetailsView = itemDetailsView;
        this.itemsRepository = new ItemsRepository(application);
    }

    @Override
    public void loadDetails(String itemId) {

        try {
            new getDetailAsyncTask(itemsRepository, itemDetailsView).execute(itemId);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateDetails(int id, String title, String cost, String location, String description, String imagePath) {
        Items update = new Items(id, title,location,description,cost,imagePath);
        itemsRepository.update(update);
        itemDetailsView.itemUpdated();
    }

    @Override
    public void deleteById(String id) {
        itemsRepository.deleteById(id);
        itemDetailsView.itemDeleted(id);
    }

    @Override
    public void selectImageFromCamera(Context context) {
        pickImageFromSource(Sources.CAMERA,context);
    }

    @Override
    public void selectImageFromGallery(Context context) {
        pickImageFromSource(Sources.GALLERY, context);
    }

    private void pickImageFromSource(Sources source, Context context) {
        RxImagePicker.with(context).requestImage(source)
                .flatMap(uri -> {
                    return RxImageConverters.uriToFile(context, uri, createTempFile(context));
                })
                .subscribe(this::onImagePicked, throwable -> Toast.makeText(context, String.format("Error: %s", throwable), Toast.LENGTH_LONG).show());
    }

    private void onImagePicked(Object result) {
        itemDetailsView.setImagePath(result.toString());

    }

    private File createTempFile(Context context) {
        return new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), System.currentTimeMillis() + "_image.jpeg");
    }

    private static class getDetailAsyncTask extends AsyncTask<String,Void,Items>{

        ItemsRepository repository;
        ItemDetailsView view;

        public getDetailAsyncTask(ItemsRepository repository, ItemDetailsView view) {
            this.repository = repository;
            this.view = view;
        }

        @Override
        protected Items doInBackground(String... strings) {

            Items items = repository.getItemById(strings[0]);
//            view.showData(items);
            return items;
        }

        @Override
        protected void onPostExecute(Items items) {
            super.onPostExecute(items);
            view.showData(items);
            Log.d("ABC", "In Presenter " + items.getTitle());

        }
    }
}
