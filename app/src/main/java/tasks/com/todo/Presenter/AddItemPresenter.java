package tasks.com.todo.Presenter;

import android.app.Application;
import android.content.Context;
import android.os.Environment;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mlsdev.rximagepicker.RxImageConverters;
import com.mlsdev.rximagepicker.RxImagePicker;
import com.mlsdev.rximagepicker.Sources;

import java.io.File;
import java.util.UUID;

import tasks.com.todo.Database.ItemsRepository;
import tasks.com.todo.Model.Items;
import tasks.com.todo.View.AddItemsView;

public class AddItemPresenter implements IAddItemPresenter{

    AddItemsView view;
    Context context;
    ItemsRepository itemsRepository;

    public AddItemPresenter(Context context, AddItemsView view, Application application) {
        this.view = view;
        this.context = context;
        itemsRepository = new ItemsRepository(application);
    }

    @Override
    public void saveData(String title, String cost, String location, String description, String imagePath) {

        if(view.validateFields()){
            Items newItem = new Items(title,location,description,cost,imagePath);
            itemsRepository.insert(newItem);

//            syncItemsWithCloud(newItem);
            Toast.makeText(context,title+" Saved",Toast.LENGTH_SHORT).show();
            view.showData(newItem);
        }

    }

    @Override
    public void selectImageFromCamera(Context context) {
        pickImageFromSource(Sources.CAMERA,context);
    }

    @Override
    public void selectImageFromGallery(Context context) {
        pickImageFromSource(Sources.GALLERY, context);
    }

    private void pickImageFromSource(Sources source, Context context) {
        RxImagePicker.with(context).requestImage(source)
                .flatMap(uri -> {
                    return RxImageConverters.uriToFile(context, uri, createTempFile(context));
                })
                .subscribe(this::onImagePicked, throwable -> Toast.makeText(context, String.format("Error: %s", throwable), Toast.LENGTH_LONG).show());
    }

    private void onImagePicked(Object result) {
        if(result != null){
            view.hideView();
            view.setImagePath(result.toString());
        }
        else {
            view.showView();
        }


    }

    private File createTempFile(Context context) {
        return new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), System.currentTimeMillis() + "_image.jpeg");
    }
}
