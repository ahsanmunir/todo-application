package tasks.com.todo.Presenter;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;
import java.util.UUID;

import tasks.com.todo.Database.ItemsRepository;
import tasks.com.todo.Model.CloudItems;
import tasks.com.todo.Model.Items;
import tasks.com.todo.View.ItemsView;

public class ItemsPresenter implements iItemsPresenter {

    ItemsView itemsView;
    ItemsRepository itemsRepository;

    public ItemsPresenter(ItemsView itemsView, Application application) {
        this.itemsView = itemsView;
        itemsRepository = new ItemsRepository(application);
    }

    @Override
    public LiveData<List<Items>> loadItems() {
        itemsView.dataLoadedSuccessfully();
        return itemsRepository.getAllItems();
    }

    @Override
    public void deleteAllItems() {
        itemsView.showView();
    }

    @Override
    public void itemDetails(Items items) {
        itemsView.openDetails(String.valueOf(items.getId()));
    }

    @Override
    public void loadDataFromCloud() {
        FirebaseDatabase.getInstance().getReference().child("items").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                    CloudItems items = childSnapshot.getValue(CloudItems.class);
                    itemsRepository.insert(new Items(items.id,items.title,items.location,items.description,items.cost,items.imagePath));
                }
                itemsView.dataLoadedSuccessfully();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
