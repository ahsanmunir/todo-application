package tasks.com.todo.Presenter;

import android.arch.lifecycle.LiveData;

import java.util.List;

import tasks.com.todo.Model.Items;

public interface iItemsPresenter {

//    void loadItems();
    LiveData<List<Items>> loadItems();
    void deleteAllItems();
    void itemDetails(Items items);
    void loadDataFromCloud();
}
