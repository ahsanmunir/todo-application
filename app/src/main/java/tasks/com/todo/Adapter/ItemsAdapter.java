package tasks.com.todo.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

import tasks.com.todo.Model.CloudItems;
import tasks.com.todo.Model.Items;
import tasks.com.todo.R;

public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.MyViewHolder> {

    Context context;
    List<Items> itemsList;
    ItemsAdapterListener listener;



    public ItemsAdapter(Context context, List<Items> itemsList, ItemsAdapterListener listener) {
        this.context = context;
        this.itemsList = itemsList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.items_list, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        if (itemsList != null) {
            Items items = itemsList.get(i);
            myViewHolder.title.setText(items.getTitle());
            myViewHolder.cost.setText(String.valueOf(items.getCost()));

            try {
                if(!TextUtils.isEmpty(items.getImagePath())){
                    Glide.with(context)
                            .load(items.getImagePath())
                            .apply(RequestOptions.centerCropTransform()
                                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                            .into(myViewHolder.itemImage);
                }
            }catch (Exception e){e.printStackTrace();}
        }
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView itemImage;
        TextView title, cost;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            itemImage = (ImageView) itemView.findViewById(R.id.list_item_image);
            title = (TextView) itemView.findViewById(R.id.title);
            cost = (TextView) itemView.findViewById(R.id.cost);

            itemView.setOnClickListener(view ->
                    listener.onItemSelected(itemsList.get(getAdapterPosition())));
        }
    }

    public void setWords(List<Items> items) {
        itemsList = items;
        notifyDataSetChanged();
    }

    public interface ItemsAdapterListener {
        void onItemSelected(Items items);
    }

    public void syncData() {
        FirebaseDatabase instance = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = instance.getReference("items");

        for (Items items : itemsList) {
            databaseReference.child(String.valueOf(items.getId())).setValue(items);
        }
    }

}
