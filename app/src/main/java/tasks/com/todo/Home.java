package tasks.com.todo;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import tasks.com.todo.Adapter.ItemsAdapter;
import tasks.com.todo.Model.Items;
import tasks.com.todo.Presenter.ItemsPresenter;
import tasks.com.todo.View.ItemsView;

public class Home extends AppCompatActivity implements ItemsAdapter.ItemsAdapterListener, ItemsView {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    ItemsAdapter adapter;
    SwipeRefreshLayout swipeRefreshLayout;
    List<Items> itemsList = new ArrayList<>();
    ItemsPresenter presenter;
    @BindView(R.id.noTask)
    LinearLayout noTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // Set up the toolbar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabNewNote);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeLayout);
        swipeRefreshLayout.setColorSchemeColors(
                getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorPrimaryDark),
                getResources().getColor(R.color.colorAccent)
        );

        presenter = new ItemsPresenter(this, getApplication());

        adapter = new ItemsAdapter(this, itemsList, this);

        observer();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(adapter);

        swipeRefreshLayout.setOnRefreshListener(() -> {
            try {
                presenter.loadItems();
//                adapter.notifyDataSetChanged();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        swipeRefreshLayout.post(() -> {
            try {
                presenter.loadItems();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        noTask.setOnClickListener(view -> startActivity(new Intent(Home.this, AddItemsActivity.class)));

        fab.setOnClickListener(view -> startActivity(new Intent(Home.this, AddItemsActivity.class)));
    }

    @Override
    public void onItemSelected(Items items) {
        presenter.itemDetails(items);
    }

    @Override
    public void dataLoadedSuccessfully() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void openDetails(String id) {
        Intent intent = new Intent(Home.this, ItemDetailActivity.class);
        intent.putExtra("itemID", id);
        startActivity(intent);
    }

    @Override
    public void hideView() {
        noTask.setVisibility(View.GONE);
    }

    @Override
    public void showView() {
        noTask.setVisibility(View.VISIBLE);
    }

    private void observer() {
        try {
            presenter.loadItems().observe(this, items -> {
                if (items.size() > 0) {
                    adapter.setWords(items);
                    adapter.syncData();
//                    presenter.loadDataFromCloud();
                    hideView();
                } else {
                    presenter.loadDataFromCloud();
                    showView();
                    Toast.makeText(Home.this, "Empty", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_item_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        if(item.getItemId() == android.R.id.home){
//            finish();
//        }
        switch (item.getItemId()) {
            case R.id.sync:
//                adapter.syncData();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
