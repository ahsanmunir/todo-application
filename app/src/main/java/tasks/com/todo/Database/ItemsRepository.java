package tasks.com.todo.Database;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.util.Log;

import java.util.List;

import tasks.com.todo.Model.Items;

public class ItemsRepository {

    private ItemsDao itemsDao;
    private LiveData<List<Items>> allItems;
    private static Items currentItems;

    public ItemsRepository(Application application) {
        ItemsRoomDatabase db = ItemsRoomDatabase.getINSTANCE(application);
        this.itemsDao = db.itemsDao();
        this.allItems = itemsDao.getAllItems();
    }

    public  LiveData<List<Items>> getAllItems(){return allItems;}

    public void insert(Items items){ new insertAsyncTask(itemsDao).execute(items);
//        Log.d("insert", String.valueOf(items.getId()));
    }

    public void update(Items items){ new updateAsyncTask(itemsDao).execute(items); }

    public void deleteAll(){ new deleteAllAsyncTask(itemsDao).execute(); }

    public void deleteById(String itemId){new deleteByIdAsyncTask(itemsDao).execute(itemId); }

    public Items getItemById(String itemId){
//        new getByIdAsyncTask(itemsDao).execute(itemId);
        currentItems = itemsDao.getItemById(itemId);
        Log.d("ABC", "In Repo fun " + currentItems.getTitle());
        return currentItems;
    }

    private static class insertAsyncTask extends AsyncTask<Items, Void, Void> {

        private ItemsDao mAsyncTaskDao;

        insertAsyncTask(ItemsDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Items... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    private static class updateAsyncTask extends AsyncTask<Items, Void, Void> {

        private ItemsDao mAsyncTaskDao;

        updateAsyncTask(ItemsDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Items... params) {
            mAsyncTaskDao.update(params[0]);
            return null;
        }
    }

    private static class deleteAllAsyncTask extends AsyncTask<Items, Void, Void> {

        private ItemsDao mAsyncTaskDao;

        deleteAllAsyncTask(ItemsDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Items... params) {
            mAsyncTaskDao.deleteAll();
            return null;
        }
    }

    private static class deleteByIdAsyncTask extends AsyncTask<String, Void, Void> {

        private ItemsDao mAsyncTaskDao;

        deleteByIdAsyncTask(ItemsDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(String... strings) {
            mAsyncTaskDao.deleteItemById(strings[0]);
            return null;
        }
    }

//    private static class getByIdAsyncTask extends AsyncTask<String, Items, Items> {
//
//        private ItemsDao mAsyncTaskDao;
//
//        getByIdAsyncTask(ItemsDao dao) {
//            mAsyncTaskDao = dao;
//        }
//
//        @Override
//        protected Items doInBackground(String... strings) {
//            currentItems = mAsyncTaskDao.getItemById(strings[0]);
//            Log.d("ABC", "In Repo Async " + currentItems.getTitle());
//            return currentItems;
//        }
//
//    }

}
