package tasks.com.todo.Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import tasks.com.todo.Model.Items;

@Database(entities = {Items.class}, version = 1, exportSchema = false)
public abstract class ItemsRoomDatabase extends RoomDatabase {

    public static ItemsRoomDatabase INSTANCE;

    public abstract ItemsDao itemsDao();

    public static ItemsRoomDatabase getINSTANCE(Context context){
        if(INSTANCE == null){
            synchronized (ItemsRoomDatabase.class){
                if(INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            ItemsRoomDatabase.class, "items_database")
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }

}
