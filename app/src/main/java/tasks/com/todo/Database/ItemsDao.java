package tasks.com.todo.Database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import tasks.com.todo.Model.Items;

@Dao
public interface ItemsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Items items);

    @Update
    void update(Items... items);

    @Query("Select * from items order by id Desc")
    LiveData<List<Items>> getAllItems();

    @Query("Select * from items where id = :itemId")
    Items getItemById(String itemId);

    @Query("Delete from items")
    void deleteAll();

    @Query("DELETE FROM items WHERE id = :itemId")
    int deleteItemById(String itemId);
}
