package tasks.com.todo.Model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.UUID;

@Entity(tableName = "items")
public class Items {

    @PrimaryKey(autoGenerate = true)
//    @PrimaryKey
    @NonNull
    private int id;

    @Nullable
    String title;

    @Nullable
    String location;

    @Nullable
    String description;

    @Nullable
    String imagePath;

    @Nullable
    String cost;

    public Items(String title, String location, String description, String cost, String imagePath) {
        this.title = title;
        this.location = location;
        this.description = description;
        this.cost = cost;
        this.imagePath = imagePath;
//        this.id = Integer.parseInt(UUID.randomUUID().toString());
    }

    @Ignore
    public Items(int id, String title, String location, String description, String cost, String imagePath) {
        this.title = title;
        this.location = location;
        this.description = description;
        this.cost = cost;
        this.imagePath = imagePath;
        this.id = id;
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    @Nullable
    public String getTitle() {
        return title;
    }

    public void setTitle(@Nullable String title) {
        this.title = title;
    }

    @Nullable
    public String getLocation() {
        return location;
    }

    public void setLocation(@Nullable String location) {
        this.location = location;
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    public void setDescription(@Nullable String description) {
        this.description = description;
    }

    @Nullable
    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(@Nullable String imagePath) {
        this.imagePath = imagePath;
    }

    @Nullable
    public String getCost() {
        return cost;
    }

    public void setCost(@Nullable String cost) {
        this.cost = cost;
    }
}
