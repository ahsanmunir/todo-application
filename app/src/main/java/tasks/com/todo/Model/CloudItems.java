package tasks.com.todo.Model;

import android.support.annotation.Keep;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class CloudItems {


    public int id;

    public String title;

    public String location;

    public String description;

    public String imagePath;

    public String cost;

    @Keep
    public CloudItems() {

    }

    @Keep
    public CloudItems(int id, String title, String location, String description, String cost, String imagePath) {
        this.title = title;
        this.location = location;
        this.description = description;
        this.cost = cost;
        this.imagePath = imagePath;
        this.id = id;
    }

}
