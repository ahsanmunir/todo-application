package tasks.com.todo.View;

import android.content.ClipData;

import java.util.List;

import tasks.com.todo.Model.Items;

public interface ItemsView {

    void dataLoadedSuccessfully();
    void openDetails(String id);
    void hideView();
    void showView();
}
