package tasks.com.todo.View;

import android.content.ClipData;

import tasks.com.todo.Model.Items;

public interface AddItemsView {

    void showData(Items items);

    void setImagePath(String imagePath);

    boolean validateFields();

    void showView();

    void hideView();
}
