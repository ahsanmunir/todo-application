package tasks.com.todo.View;

import tasks.com.todo.Model.Items;

public interface ItemDetailsView {

    void showData(Items items);
    void itemDeleted(String id);
    void itemUpdated();
    void setImagePath(String path);
}
