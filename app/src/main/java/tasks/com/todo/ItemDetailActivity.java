package tasks.com.todo;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;
import tasks.com.todo.Adapter.ItemsAdapter;
import tasks.com.todo.Model.Items;
import tasks.com.todo.Presenter.ItemDetailPresenter;
import tasks.com.todo.View.ItemDetailsView;

public class ItemDetailActivity extends AppCompatActivity implements ItemDetailsView {

    @BindView(R.id.title) EditText title;
    @BindView(R.id.cost) EditText cost;
    @BindView(R.id.location) EditText location;
    @BindView(R.id.description) EditText description;
    @BindView(R.id.itemPicture) ImageView imageView;
    @BindView(R.id.addImage) ImageView imagePicker;
    ItemDetailPresenter presenter;

    String taskId, imagePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);
        ButterKnife.bind(this);

        // Set up the toolbar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowHomeEnabled(true);

        // Get the requested task id
        taskId = getIntent().getStringExtra("itemID");
        if(TextUtils.isEmpty(taskId)){
            ab.setTitle("Details");
        }else {
            ab.setTitle(taskId);
        }

        presenter = new ItemDetailPresenter(this,getApplication());
        presenter.loadDetails(taskId);

        imagePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImageOption();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.item_detail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        switch (item.getItemId()) {
            case R.id.save:
                presenter.updateDetails(Integer.parseInt(taskId), title.getText().toString(), cost.getText().toString(), location.getText().toString(), description.getText().toString(), imagePath);
                return true;
            case R.id.delete:
                presenter.deleteById(taskId);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showData(Items items) {
        title.setText(items.getTitle());
        cost.setText(items.getCost());
        location.setText(items.getLocation());
        description.setText(items.getDescription());
        Glide.with(this)
                .load(items.getImagePath())
                .into(imageView);
    }

    @Override
    public void itemDeleted(String id) {
        FirebaseDatabase instance = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = instance.getReference("items");

        databaseReference.child(id).removeValue();

        Toast.makeText(ItemDetailActivity.this, "Item Deleted Successfully", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void itemUpdated() {
        Toast.makeText(ItemDetailActivity.this, "Item Updated Successfully", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void setImagePath(String path) {
        imagePath = path;
        if (!imagePath.isEmpty() && this != null) {
            Glide.with(this)
                    .load(imagePath)
                    .into(imageView);
        }
    }

    private void selectImageOption () {
//            final AppCompatDialog customDialog;
        Dialog customDialog;
        try {
//                customDialog = new AppCompatDialog(this);
            customDialog = new Dialog(this);
            customDialog.setContentView(R.layout.custom_dialog);

            final Button select_camera = (Button) customDialog.findViewById(R.id.camera);
            final Button select_gallery = (Button) customDialog.findViewById(R.id.gallery);
            ImageView close = (ImageView) customDialog.findViewById(R.id.closeFound);


            select_camera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.selectImageFromCamera(ItemDetailActivity.this);
                    customDialog.cancel();
                }
            });
            select_gallery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.selectImageFromGallery(ItemDetailActivity.this);
                    customDialog.cancel();
                }
            });
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    customDialog.dismiss();
                }
            });
            customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            customDialog.setCancelable(false);
            customDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
