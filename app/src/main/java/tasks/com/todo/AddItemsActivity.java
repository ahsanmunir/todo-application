package tasks.com.todo;

import android.app.Dialog;
import android.content.ClipData;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tasks.com.todo.Model.Items;
import tasks.com.todo.Presenter.AddItemPresenter;
import tasks.com.todo.View.AddItemsView;

public class AddItemsActivity extends AppCompatActivity implements AddItemsView {

    @BindView(R.id.title) EditText title;
    @BindView(R.id.cost) EditText cost;
    @BindView(R.id.location) EditText location;
    @BindView(R.id.description) EditText description;
    @BindView(R.id.itemPicture) ImageView imageView;
    @BindView(R.id.addImage) ImageView imagePlus;
    @BindView(R.id.imageText) TextView imageText;
    @BindView(R.id.imagePicker) LinearLayout imagePicker;
//    @BindView(R.id.remove) Button imageRemove;

    String imagePath;

    AddItemPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_items);
        ButterKnife.bind(this);

        // Set up the toolbar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowHomeEnabled(true);

        presenter = new AddItemPresenter(this, this, getApplication());

        imagePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImageOption();
            }
        });

//        imageRemove.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                imageView.setImageDrawable(null);
//                showView();
//            }
//        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.add_item_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        switch (item.getItemId()) {
            case R.id.save:
                presenter.saveData(title.getText().toString(), cost.getText().toString(), location.getText().toString(), description.getText().toString(), imagePath);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showData(Items items) {
        finish();
//        Snackbar.make(linearLayout,items.getTitle(),Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void setImagePath(String path) {
        imagePath = path;
        if (!imagePath.isEmpty() && this != null) {
            Glide.with(this)
                    .load(imagePath)
                    .into(imageView);
        }
    }

    @Override
    public boolean validateFields() {
        if(TextUtils.isEmpty(title.getText().toString())){
            title.setError("Title cannot be empty");
            return false;
        }else{
            return true;
        }
    }

    @Override
    public void showView() {
        imagePlus.setVisibility(View.VISIBLE);
        imageText.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideView() {
        imagePlus.setVisibility(View.GONE);
        imageText.setVisibility(View.GONE);
//        imageRemove.setVisibility(View.VISIBLE);
    }

    private void selectImageOption () {
//            final AppCompatDialog customDialog;
            Dialog customDialog;
            try {
//                customDialog = new AppCompatDialog(this);
                customDialog = new Dialog(this);
                customDialog.setContentView(R.layout.custom_dialog);

                final Button select_camera = (Button) customDialog.findViewById(R.id.camera);
                final Button select_gallery = (Button) customDialog.findViewById(R.id.gallery);
                ImageView close = (ImageView) customDialog.findViewById(R.id.closeFound);


                select_camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        presenter.selectImageFromCamera(AddItemsActivity.this);
                        customDialog.cancel();
                    }
                });
                select_gallery.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        presenter.selectImageFromGallery(AddItemsActivity.this);
                        customDialog.cancel();
                    }
                });
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        customDialog.dismiss();
                    }
                });
                customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                customDialog.setCancelable(false);
                customDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
}
